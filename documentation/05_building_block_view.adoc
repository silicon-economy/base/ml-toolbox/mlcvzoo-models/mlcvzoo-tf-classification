[[section-building-block-view]]

== Building Block View

=== Overview

==== mlcvzoo_tf_classification module (Blackbox)

The *mlcvzoo_tf_classification* module handles the wrapping of classification algorithms that are implemented using tensorflow. For now a MLCVZoo custom architecture and the XceptionNet are provided. A more generic
wrapping of tensorflow classifications algorithms is planned for future versions of the MLCVZoo.

=== Building Blocks - Level 2

==== mlcvzoo_tf_classification package (Whitebox)

[#mlcvzoo-tf-classification-whitebox]
.Overview of the mlcvzoo_tf_classification packages as Whitebox View
image::images/05_mlcvzoo-tf-classification-package-structure.png[image]

There are the following components with its responsibilities:

[cols="2",options="header"]
|===
|Name
|Responsibility / Description

|custom_block
|Package that handles the implementation of the CustomBlock model.
A model for image classification.

|xception

|Package that handles the wrapper implementation of the tensorflow Xception model.
A model for image classification.

|annotation_utils

|Component for handling annotation utility methods that are used across the
mlcvzoo_tf_classification package.

|base_model

|Component for handling the implementation of a generic base model that wraps tensorflow
classification models.

|configuration

|Definition of the Config that is used to configure the models of the mlcvzoo_tf_classification package.

|const

|Component for enumerating options for different configurations for different parameters.
The constant classes listed here are e.g. used in data generators to
limit the options in configuration.

|image_generator


|Component for handling the generation of images for the training of
tensorflow classification models.

|===

===== mlcvzoo_tf_classification.custom_block package
Package that contains all classes that are needed to implement a custom tensorflow model. The _CustomBlockNet_ defines a custom neural network by utilizing tensorflow layers. The _CustomBlockModel_ is the wrapper class for the whole classification algorithm that is conform to these MLCVZoo api.

===== mlcvzoo_tf_classification.xception package
Package that contains all classes that are needed to implement a wrapper for https://www.tensorflow.org/api_docs/python/tf/keras/applications/xception/Xception[tensorflow's Xception] model implementation. While the _XceptionNet_ wraps the neural network architecture, the _XceptionModel_ wraps the whole classfication model that is conform to the api of the MLCVZoo.
