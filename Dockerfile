FROM nexus.apps.sele.iml.fraunhofer.de/sele/ml-toolbox/cicd/mlcvzoo-ci-image:latest

# Define paths. Let all path definitions always end with a '/'!

# Path to all code we will place in this container image
ENV BUILD_ENV_DIR="/build-env/"
# Path to the actual project
ENV PROJECT_DIR="${BUILD_ENV_DIR}MLCVZoo/mlcvzoo-tf-classification/"
# Create those locations
RUN mkdir -p "$PROJECT_DIR"

COPY pyproject.toml "$PROJECT_DIR"
COPY poetry.lock "$PROJECT_DIR"

WORKDIR "$PROJECT_DIR"

ENV VIRTUAL_ENV="${BUILD_ENV_DIR}venv"
ENV PATH="${VIRTUAL_ENV}bin:$PATH"
RUN python3 -m venv "$VIRTUAL_ENV" \
  && poetry run pip install --upgrade pip \
  && poetry install --no-interaction --no-ansi --no-root

# Cleanup poetry cache
RUN rm -rf ~/.cache/pypoetry

# We don't deliver unit tests so even without --no-root we would need to set the PYTHONPATH here
ENV PYTHONPATH="$PYTHONPATH:$PROJECT_DIR"

# ====================================================================
# Label the image
LABEL org.opencontainers.image.authors="Maximilian Otten <maximilian.otten@iml.fraunhofer.de>, Christian Hoppe <christian.hoppe@iml.fraunhofer.de" \
      org.opencontainers.image.vendor="Fraunhofer IML" \
      org.opencontainers.image.title="MLCVZoo TF-Classification - Main GPU-enabled gitlab-runner container" \
      org.opencontainers.image.description="Container image for GPU enabled integration testing and continuous delivery for MLCVZoo TF-Classification"
