# MLCVZoo mlcvzoo_tf_classification module Versions:

6.0.3 (2024-02-07):
------------------
Updated links in pyproject.toml

6.0.2 (2023-06-15):)
------------------
Relicense to OLFL-1.3 which succeeds the previous license

6.0.1 (2023-05-03):
------------------
Python 3.10 compatibility

6.0.0 (2023-02-14):)
------------------
Implement API changes introduces by mlcvzoo-base version 5.0.0
- Remove detector-config and use the feature of the single ModelConfiguration
- Remove duplicate attributes

5.0.1 (2022-09-09):
------------------
Ensure ConfigBuilder version 7 compatibility

5.0.0 (2022-08-08):
------------------
Adapt to mlcvzoo-base 4.0.0: Remove the net classes and move the functionality
to the respective models

4.0.1 (2022-07-11):
------------------
Prepare package for PyPi

4.0.0 (2022-07-08):
------------------
Added functionality - Saving of training checkpoints to a directory:
- Added ModelCheckpointConfig that configures a callback which is executed during
  training to save checkpoints
- Added option for saving weights or full model state in checkpoints by
  introducing "save_weights_only" parameter in net_config, so also restoring a saved
  state to a net is possible
- Removed Functional class from the Net classes' inheritance due to
  issues during model saving and finally because of deprecation of tf.python.keras module
- Switched all imports of tf.python.keras module to tf.keras module
- Changed saving/loading procedure in store and restore functions of the Net classes
- Added tests for saving and loading checkpoints and full model

3.0.0 (2022-05-16):
------------------
Use new features from AnnotationClassMapper that have been added with mlcvzoo_base v3.0.0

2.0.0 (2022-04-05)
------------------
- initial release of the package
