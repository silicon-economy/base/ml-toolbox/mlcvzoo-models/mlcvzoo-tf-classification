# MLCVZoo TF Classification

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_tf_classification** is the wrapper module
for classification algorithms that are implemented using tensorflow.

## Install
`
pip install mlcvzoo-tf-classification
`

## Technology stack

- Python
