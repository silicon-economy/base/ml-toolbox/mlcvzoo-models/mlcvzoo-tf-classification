# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""
Test class for XceptionModel class.
"""

import logging
import os
from typing import Union, cast

import cv2
import numpy as np
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from mlcvzoo_base.api.data.classification import Classification
from related import to_model

from mlcvzoo_tf_classification.tests.test_template import TestTemplate
from mlcvzoo_tf_classification.xception.configuration import XceptionConfig
from mlcvzoo_tf_classification.xception.model import XceptionModel

logger = logging.getLogger(__name__)


class TestXceptionModel(TestTemplate):
    """
    Test class for XceptionModel class.
    """

    def __xception_predict_and_evaluate(
        self, data_item: Union[np.ndarray, str]
    ) -> None:
        """
        Calls the model's predict method and runs evaluation of prediction for
        a given data item.
        """

        yaml_path = os.path.join(
            self.project_root,
            "test_data",
            "test_Xception",
            "xception_imagenet_test.yaml",
        )

        xception_model = XceptionModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )

        _, predictions = xception_model.predict(data_item)

        logger.info(
            "predictions:\n" "amount %s, type: %s\n" "first ten entries: %s,\n",
            len(predictions),
            type(predictions),
            predictions[:10],
        )

        expected_predictions = []

        # 1st score
        expected_prediction = Classification(
            class_identifier=ClassIdentifier(
                class_name="spotlight, spot",
                class_id=818,
            ),
            score=0.05,
        )
        expected_predictions.append(expected_prediction)

        # 2nd score
        expected_prediction = Classification(
            class_identifier=ClassIdentifier(
                class_name="matchstick",
                class_id=644,
            ),
            score=0.04,
        )
        expected_predictions.append(expected_prediction)

        # 3rd score
        expected_prediction = Classification(
            class_identifier=ClassIdentifier(
                class_name="nematode, nematode worm, roundworm",
                class_id=111,
            ),
            score=0.037,
        )
        expected_predictions.append(expected_prediction)

        # 4th score
        expected_prediction = Classification(
            class_identifier=ClassIdentifier(
                class_name="digital clock",
                class_id=530,
            ),
            score=0.026,
        )

        expected_predictions.append(expected_prediction)

        # 5th score
        expected_prediction = Classification(
            class_identifier=ClassIdentifier(
                class_name="jack-o'-lantern",
                class_id=607,
            ),
            score=0.024,
        )
        expected_predictions.append(expected_prediction)

        # TODO: __eq__ equal method is not implemented, therefore this operator does not work
        # is_correct = expected_prediction == predictions[0]

        is_correct = True
        wrong_prediction_at_index = None
        # checks only top 5 predictions
        for index, prediction in enumerate(expected_predictions):
            is_correct = (
                is_correct
                and prediction.class_id == predictions[index].class_id
                and prediction.class_name == predictions[index].class_name
            )
            if not is_correct:
                wrong_prediction_at_index = index

        if not is_correct:
            logger.warning(
                "Found wrong prediction: \n" "  Expected:  %s\n" "  Predicted: %s\n",
                expected_predictions[wrong_prediction_at_index],  # type: ignore
                predictions[wrong_prediction_at_index],
            )

            logger.warning(
                "All predictions: \n" "expected: \n %s \n" "predicted: \n %s ",
                expected_predictions,
                predictions[:5],
            )

            raise ValueError("Output is not valid!")

        del xception_model

    def test_configuration(self):
        configuration = XceptionModel.create_configuration(
            from_yaml=os.path.join(
                self.project_root,
                "test_data",
                "test_Xception",
                "xception_from_scratch_test.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        assert configuration is not None

        configuration_2 = cast(
            XceptionConfig, to_model(XceptionConfig, configuration.to_dict())
        )
        assert configuration_2 is not None

    def test_xception_inference_from_file(self) -> None:
        """Tests whether prediction from a file works"""

        test_image_path = os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )

        self.__xception_predict_and_evaluate(test_image_path)

    def test_xception_inference_from_array(self) -> None:
        """Tests whether prediction from a loaded data array works."""

        test_image_path = os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )

        data_item = cv2.imread(test_image_path)

        self.__xception_predict_and_evaluate(data_item)

    def test_xception_training_from_scratch(self) -> None:
        """
        Tests whether training of a model from scratch works when
        loading data from a data frame.
        """

        yaml_path = os.path.join(
            self.project_root,
            "test_data",
            "test_Xception",
            "xception_from_scratch_test.yaml",
        )

        xception_model = XceptionModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )
        xception_model.train()

    def test_xception_training(self) -> None:
        """
        Tests whether fine tuning of a pretrained model works when
        loading data from a data frame.
        """

        yaml_path = os.path.join(
            self.project_root,
            "test_data",
            "test_Xception",
            "xception_imagenet_test.yaml",
        )

        xception_model = XceptionModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )

        xception_model.train()

    def test_xception_transfer_learning_training(self) -> None:
        """
        Tests whether transfer learning of a pretrained model works when
        loading data from a data frame.
        """

        yaml_path = os.path.join(
            self.project_root,
            "test_data",
            "test_Xception",
            "xception_transfer_learning_test.yaml",
        )

        xception_model = XceptionModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )

        xception_model.train()

    def test_xception_transfer_learning_training_from_dir(self) -> None:
        """
        Tests whether transfer learning of a pretrained model works when
        loading data from a directory.
        """

        yaml_path = os.path.join(
            self.project_root,
            "test_data",
            "test_Xception",
            "xception_transfer_learning_from_dir_test.yaml",
        )

        xception_model = XceptionModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )

        xception_model.train()

        del xception_model

    def test_xception_save_and_load_model(self) -> None:
        """
        Tests whether saving of a model and resuming training
        from the saved state works.
        """

        yaml_path = os.path.join(
            self.project_root,
            "test_data",
            "test_Xception",
            "xception_imagenet_test_save_model.yaml",
        )

        xception_model = XceptionModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )

        # train model and save checkpoints along the way
        xception_model.train()

        # save model
        model_path = os.path.join(
            xception_model.configuration.train_config.model_checkpoint_config.work_dir,
            "model",
        )
        xception_model.store(checkpoint_path=model_path)

        # take predictions of the trained model
        test_image_path = os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )

        data_item = cv2.imread(test_image_path)

        # check predictions on restored model
        _, predictions_before = xception_model.predict(data_item=data_item)

        # load model
        xception_model.restore(checkpoint_path=model_path)

        # check predictions on restored model
        _, predictions_after = xception_model.predict(data_item=data_item)

        # For this test check only the first two predictions
        is_correct = True
        wrong_prediction_at_index = None
        # checks only top 2 predictions
        for index, prediction in enumerate(predictions_after):
            is_correct = (
                is_correct
                and prediction.class_id == predictions_before[index].class_id
                and prediction.class_name == predictions_before[index].class_name
            )
            if not is_correct:
                wrong_prediction_at_index = index

        if not is_correct:
            logger.warning(
                "Found wrong prediction: \n" "  Expected:  %s\n" "  Predicted: %s\n",
                predictions_before[wrong_prediction_at_index].class_id,  # type: ignore
                predictions_after[wrong_prediction_at_index].class_id,
            )

            logger.warning(
                "All predictions: \n" "expected: \n %s \n" "predicted: \n %s ",
                predictions_before,
                predictions_after[:2],
            )

            raise ValueError("Output is not valid!")

    def test_xception_save_and_load_weights(self) -> None:
        """
        Tests whether saving weights of a model and inference
        with restored weights works.
        """

        yaml_path = os.path.join(
            self.project_root,
            "test_data",
            "test_Xception",
            "xception_imagenet_test.yaml",
        )

        xception_model = XceptionModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )

        # train model and save checkpoints along the way
        xception_model.train()

        # save model
        model_path = os.path.join(
            xception_model.configuration.train_config.model_checkpoint_config.work_dir,
            "test-checkpoint",
        )
        xception_model.store(checkpoint_path=model_path)

        # take predictions of the trained model
        test_image_path = os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )

        data_item = cv2.imread(test_image_path)

        # check predictions on restored model
        _, predictions_before = xception_model.predict(data_item=data_item)

        # load model
        xception_model.restore(checkpoint_path=model_path)

        # check predictions on restored model
        _, predictions_after = xception_model.predict(data_item=data_item)

        # For this test check only the first two predictions
        is_correct = True
        wrong_prediction_at_index = None
        # checks only top 2 predictions
        for index, prediction in enumerate(predictions_after):
            is_correct = (
                is_correct
                and prediction.class_id == predictions_before[index].class_id
                and prediction.class_name == predictions_before[index].class_name
            )
            if not is_correct:
                wrong_prediction_at_index = index

        if not is_correct:
            logger.warning(
                "Found wrong prediction: \n" "  Expected:  %s\n" "  Predicted: %s\n",
                predictions_before[wrong_prediction_at_index].class_id,  # type: ignore
                predictions_after[wrong_prediction_at_index].class_id,
            )

            logger.warning(
                "All predictions: \n" "expected: \n %s \n" "predicted: \n %s ",
                predictions_before,
                predictions_after[:2],
            )

            raise ValueError("Output is not valid!")

    def test_xception_save_and_load_checkpoints(self) -> None:
        """
        Tests whether saving checkpoints containing weights of a model and resuming
        training from the saved state works.
        """

        yaml_path = os.path.join(
            self.project_root,
            "test_data",
            "test_Xception",
            "xception_imagenet_test.yaml",
        )

        xception_model = XceptionModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )

        # train model and save checkpoints along the way
        xception_model.train()

        # take predictions of the trained model
        test_image_path = os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )

        data_item = cv2.imread(test_image_path)

        # check predictions on restored model
        _, predictions_before = xception_model.predict(data_item=data_item)

        # load model from latest checkpoint
        checkpoint_id = "{0:04d}".format(
            xception_model.configuration.train_config.epochs
        )
        checkpoint_path = os.path.join(
            xception_model.configuration.train_config.model_checkpoint_config.work_dir,
            "cp-" + checkpoint_id + ".ckpt",
        )

        xception_model.restore(checkpoint_path=checkpoint_path)

        # check predictions on restored model
        _, predictions_after = xception_model.predict(data_item=data_item)

        # For this test check only the first two predictions
        is_correct = True
        wrong_prediction_at_index = None
        # checks only top 2 predictions
        for index, prediction in enumerate(predictions_after):
            is_correct = (
                is_correct
                and prediction.class_id == predictions_before[index].class_id
                and prediction.class_name == predictions_before[index].class_name
            )
            if not is_correct:
                wrong_prediction_at_index = index

        if not is_correct:
            logger.warning(
                "Found wrong prediction: \n" "  Expected:  %s\n" "  Predicted: %s\n",
                predictions_before[wrong_prediction_at_index].class_id,  # type: ignore
                predictions_after[wrong_prediction_at_index].class_id,
            )

            logger.warning(
                "All predictions: \n" "expected: \n %s \n" "predicted: \n %s ",
                predictions_before,
                predictions_after[:2],
            )

            raise ValueError("Output is not valid!")

    def test_xception_save_and_load_model_checkpoints(self) -> None:
        """
        Tests whether saving full model checkpoints of a model and resuming
        training from the saved state works.
        """

        yaml_path = os.path.join(
            self.project_root,
            "test_data",
            "test_Xception",
            "xception_imagenet_test_save_model.yaml",
        )

        xception_model = XceptionModel(
            from_yaml=yaml_path, string_replacement_map=self.string_replacement_map
        )

        # train model and save checkpoints along the way
        xception_model.train()

        # take predictions of the trained model
        test_image_path = os.path.join(
            self.project_root, "test_data/images/dummy_task/cars.jpg"
        )

        data_item = cv2.imread(test_image_path)

        # check predictions on restored model
        _, predictions_before = xception_model.predict(data_item=data_item)

        # load model from latest checkpoint
        checkpoint_id = "{0:04d}".format(
            xception_model.configuration.train_config.epochs
        )
        checkpoint_path = os.path.join(
            xception_model.configuration.train_config.model_checkpoint_config.work_dir,
            "cp-" + checkpoint_id + ".ckpt",
        )

        xception_model.restore(checkpoint_path=checkpoint_path)

        # check predictions on restored model
        _, predictions_after = xception_model.predict(data_item=data_item)

        # For this test check only the first two predictions
        is_correct = True
        wrong_prediction_at_index = None
        # checks only top 2 predictions
        for index, prediction in enumerate(predictions_after):
            is_correct = (
                is_correct
                and prediction.class_id == predictions_before[index].class_id
                and prediction.class_name == predictions_before[index].class_name
            )
            if not is_correct:
                wrong_prediction_at_index = index

        if not is_correct:
            logger.warning(
                "Found wrong prediction: \n" "  Expected:  %s\n" "  Predicted: %s\n",
                predictions_before[wrong_prediction_at_index].class_id,  # type: ignore
                predictions_after[wrong_prediction_at_index].class_id,
            )

            logger.warning(
                "All predictions: \n" "expected: \n %s \n" "predicted: \n %s ",
                predictions_before,
                predictions_after[:2],
            )

            raise ValueError("Output is not valid!")


# TODO: add test for flow from directory config

# TODO: add tests for using inference_config.top and inference_config.score_threshold
